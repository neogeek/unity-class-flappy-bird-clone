﻿using UnityEngine;

public class CameraFollow: MonoBehaviour {

	public GameObject player;

	void Update () {

		if (player) {

			gameObject.transform.position = new Vector3(
				player.transform.position.x + 5,
				gameObject.transform.position.y,
				gameObject.transform.position.z
			);

		}

	}

}
