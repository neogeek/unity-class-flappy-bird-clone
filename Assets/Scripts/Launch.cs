﻿using UnityEngine;

public class Launch : MonoBehaviour {

	public GameObject particleExplosion;
	public GameObject particleTrail;
	public GameObject[] sections;

	private float thrust = 20.0f;
	private float speed = 60.0f;

	private ParticleSystem trail;

	private Rigidbody2D rb;

	private float spawnX = 0.0f;
	private float spawnWidth = 22.0f;

	void Awake () {

		rb = gameObject.GetComponent<Rigidbody2D>();

		GameObject obj = Instantiate(particleTrail, Vector2.zero, Quaternion.identity);
		obj.transform.parent = gameObject.transform;
		obj.transform.localPosition = new Vector3(0, 0, 1);

		trail = obj.GetComponent<ParticleSystem>();

	}

	void Start () {

		rb.AddForce(transform.right * speed);

	}

	void Update () {

		if (Input.GetKey("space") || Input.GetMouseButton(0)) {

			rb.AddForce(transform.up * thrust);

		}

	}

	void OnTriggerEnter2D (Collider2D other) {

		if (other.gameObject.tag == "SectionSpawnTrigger") {

			spawnX = spawnX + spawnWidth;

			Instantiate(sections[Random.Range(0, sections.Length)], new Vector3(spawnX, 0, 0), Quaternion.identity);

		} else {

			Instantiate(particleExplosion, gameObject.transform.position, Quaternion.identity);

			GameObject.Find("LevelController").GetComponent<LevelController>().Reset();

			Destroy(gameObject);

		}

	}

}
