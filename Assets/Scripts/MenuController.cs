﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuController : MonoBehaviour {

	public GUIStyle playStyle;
	public GUIStyle quitStyle;

	void OnGUI () {

		if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 - 50, 200, 100), "", playStyle)) {

			SceneManager.LoadScene(0);

		}

		if (GUI.Button(new Rect(Screen.width / 2 - 100, Screen.height / 2 + 50, 200, 100), "", quitStyle)) {

			#if UNITY_EDITOR

			UnityEditor.EditorApplication.isPlaying = false;

			#else

			Application.Quit();

			#endif

		}

	}

}
