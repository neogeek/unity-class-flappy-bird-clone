﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour {

	public GameObject player;
	public GUIText distanceNumber;

	public GUIStyle scoreStyle;
	public GUIStyle textStyle;

	private float currentDistance = 0;
	private float highScore = 0;
	private bool isDead = false;
	private float playerStartPosition;

	void Start () {

		#if UNITY_EDITOR

		highScore = 0;

		#else

		highScore = PlayerPrefs.GetFloat("highScore", 0);

		#endif

		playerStartPosition = player.transform.position.x;

	}

	void Update () {

		if (player) {

			currentDistance = Mathf.Floor(player.transform.position.x - playerStartPosition);

			distanceNumber.text = currentDistance.ToString();

		}

	}

	public void Reset () {

		isDead = true;

		StartCoroutine(ResetAfterDelay());

	}

	void OnGUI () {

		if (isDead) {

			GUI.Box(new Rect(50, 50, Screen.width - 100, Screen.height - 100), "", scoreStyle);

			if (currentDistance > highScore) {

				GUI.Label(new Rect(50, 60, Screen.width - 100, Screen.height - 100), "Congrats! New High Score!", textStyle);
				GUI.Label(new Rect(50, 150, Screen.width - 100, Screen.height - 100), currentDistance.ToString(), textStyle);

			} else {

				GUI.Label(new Rect(50, 60, Screen.width - 100, Screen.height - 100), "You are better than this!", textStyle);
				GUI.Label(new Rect(50, 150, Screen.width - 100, Screen.height - 100), currentDistance.ToString(), textStyle);

			}

		}

	}

	IEnumerator ResetAfterDelay () {

		yield return new WaitForSeconds(2);

		if (currentDistance > highScore) {

			highScore = currentDistance;

			PlayerPrefs.SetFloat("highScore", highScore);

		}

		SceneManager.LoadScene(0);

	}

}
